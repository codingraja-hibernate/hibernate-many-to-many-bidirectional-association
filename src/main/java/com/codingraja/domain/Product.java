package com.codingraja.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="product_master1")
public class Product {
	private Long id;
	private String name;
	private String model;
	private String brand;
	private Double price;
	private Set<Color> colors;
	
	public Product(){}
	public Product(String name, String model, String brand, Double price, Set<Color> colors) {
		super();
		this.name = name;
		this.model = model;
		this.brand = brand;
		this.price = price;
		this.colors = colors;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="model")
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
	@Column(name="brand")
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(name="product_color", 
			   joinColumns={@JoinColumn(columnDefinition="product_id")},
			   inverseJoinColumns={@JoinColumn(columnDefinition="color_id")})
	public Set<Color> getColors() {
		return colors;
	}
	public void setColors(Set<Color> colors) {
		this.colors = colors;
	}
	
	@Override
	public String toString() {
		return "Name: "+name+", Price: "+price;
	}
}
